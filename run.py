import src.data_preparation.data_preprocessor as dp
import src.data_preparation.data_loader as dl
import src.data_visualization.data_visualizer as dv
import src.data_visualization.utils as utils
from src.constans import description_reduction, description_network
from sklearn import metrics
import argparse

visualiser = dv.DataVisualizer()


def generate_2d_point_chart(data, **kwargs):
    kwargs["n_components"] = 2
    kwargs["n_jobs"] = -2
    x, labels = dp.get_reduced_data(data, **kwargs)
    print("Reduction ready")

    if kwargs["clustering_method"]:
        if kwargs["clustering_first"]:
            print("Performing clustering on original data")
            a = data
        else:
            print("Performing clustering on reduced data")
            a = x
        predicted_labels = dp.get_labels(a, **kwargs)

        if kwargs["evaluate"]:
            print("adjusted_rand_score: ",
                  metrics.adjusted_rand_score(labels, predicted_labels))
            print("adjusted_mutual_info_score: ",
                  metrics.adjusted_mutual_info_score(labels, predicted_labels))
            print("metrics.homogeneity_completeness_v_measure: ",
                  metrics.homogeneity_completeness_v_measure(labels, predicted_labels))
            print("fowlkes_mallows_score: ",
                  metrics.fowlkes_mallows_score(labels, predicted_labels))

        labels = predicted_labels
        print("clustering completed")

    visualiser.generate_2d_plot(x, labels, reg=False, as_html=kwargs["html"])
    if kwargs["show"]:
        visualiser.show_plot(blocking=True, html_name=kwargs["output_file"])
    visualiser.save_plot(kwargs["output_file"], "png")


def generate_bubble_chart(data, **kwargs):
    visualiser.bubble(*utils.get_params_for_bubble_chart(data))
    visualiser.save_plot(kwargs["output_file"])
    if kwargs["show"]:
        visualiser.show_plot(html_name=kwargs["output_file"])


def generate_violin_chart(data, **kwargs):
    y = data

    if kwargs["sort"]:
        y = y.sort_index()
        y.reindex(sorted(y.columns), axis=1)

    if kwargs["root"]:
        y = dp.remove_rows_without_given_property(y, kwargs["root"])
    y = dp.remove_zero_columns(y)

    row_names = None
    if kwargs["sample_list"]:
        row_names = [x.strip() for x in kwargs["sample_list"].split(',')]

    if kwargs["split_images"]:
        utils.generate_series_of_violin_plots(y, kwargs["output_file"], kwargs["width"], kwargs["height"])
    else:
        labels = y.columns
        y = dp.get_row_column_id_df_for_violin_plot(y, row_names, kwargs["feature_from"], kwargs["feature_to"])
        visualiser.set_width_height(kwargs["width"], kwargs["height"])
        visualiser.set_x_ticks(range(0, len(labels)), labels, rotation=90)
        visualiser.generate_violin_plot(y)
        visualiser.save_plot(kwargs["output_file"])

        if kwargs["show"]:
            visualiser.show_plot(blocking=True)

        visualiser.set_width_height(0, 0)
        visualiser.set_x_ticks(None, None, 0)


def generate_heatmap_chart(data, **kwargs):
    if kwargs["root"]:
        if kwargs["feature"]:
            data = dp.remove_rows_without_given_property(data, kwargs["root"])
        else:
            data = dp.remove_cols_without_given_property(data, kwargs["root"])

    if kwargs["sort"]:
        data = data.sort_index()
        data.reindex(sorted(data.columns), axis=1)

    data = dp.cut_matrix(data, kwargs["feature_from"], kwargs["feature_to"], kwargs["sample_from"], kwargs["sample_to"])

    if kwargs["delete_zero"]:
        data = dp.remove_zero_columns(data)

    visualiser.generate_heatmap_plot(data)
    visualiser.save_plot(kwargs["output_file"])
    if kwargs["show"]:
        visualiser.show_plot(html_name=kwargs["output_file"])


def generate_network_chart(data, **kwargs):
    args = dp.get_edges(data, kwargs["root"], kwargs["depth"], kwargs["positioning"], kwargs["feature"])
    visualiser.generate_network_chart(*args)
    if kwargs["show"]:
        visualiser.show_plot(html_name=kwargs["output_file"])
    visualiser.save_plot(kwargs["output_file"], "png")


def get_initialized_parser():
    parent_parser = argparse.ArgumentParser(add_help=False)
    parent_parser.add_argument("-m", "--max-rows", type=int, help="reduce number of rows to the given value",
                               default=None)
    parent_parser.add_argument("-o", "--output-file", type=str, help="name of an output file", default="output")
    parent_parser.add_argument("-s", "--show", action="store_true")

    parser = argparse.ArgumentParser(description="Visualisation tool")
    subparsers = parser.add_subparsers(dest="which")

    parser_reduction = subparsers.add_parser("reduction", parents=[parent_parser],
                                             help="Dimensional reduction (for help type 'run.py reduction --help')",
                                             description=description_reduction,
                                             formatter_class=argparse.RawDescriptionHelpFormatter)
    parser_reduction.add_argument("-r", "--reduction-method", type=str,
                                  help="method used for dimensional reduction (default: pca)", default="pca")
    parser_reduction.add_argument("-c", "--clustering-method", type=str,
                                  help="clustering method (no clustering if not specified")
    parser_reduction.add_argument("--html", action="store_true", help="Generate output as html")
    parser_reduction.add_argument("-n", "--n-clusters", type=int, help="Number of clusters in clustering method",
                                  default=5)
    parser_reduction.add_argument("-cf", "--clustering-first", action="store_true",
                                  help="If present, clustering will be performed before reduction")
    parser_reduction.add_argument("-e", "--evaluate", help="If specified program will print available evaluation",
                                  action="store_true")

    parser_violin = subparsers.add_parser("violin", parents=[parent_parser],
                                          help="Violin chart (for help type 'run.py violin --help')")
    parser_violin.add_argument("-r", "--root", type=str,
                               help="number or name of the feature from which filtering should start (default: None)",
                               default=None)
    parser_violin.add_argument("--width", type=int, help="image size - width (default: 40)", default=40)
    parser_violin.add_argument("--height", type=int, help="image size - height (default: 12)", default=12)
    parser_violin.add_argument("-ff", "--feature-from", type=int,
                               help="", default=0)
    parser_violin.add_argument("-ft", "--feature-to", type=int,
                               help="", default=None)
    parser_violin.add_argument("-sl", "--sample-list", type=str,
                               help="comma separated list of names or numbers of samples", default=None)
    parser_violin.add_argument("-si", "--split-images", action="store_true",
                               help="If present, program will generate a lot of violin images with reasonable"
                                    "number of feature and samples per one (-s will be omitted")
    parser_violin.add_argument("-S", "--sort", action="store_true",
                               help="sort samples")

    parser_bubble = subparsers.add_parser("bubble", parents=[parent_parser],
                                          help="Bubble chart (for help type 'run.py bubble --help')")

    parser_heatmap = subparsers.add_parser("heatmap", parents=[parent_parser],
                                           help="Heatmap chart (for help type 'run.py heatmap --help')")
    parser_heatmap.add_argument("-r", "--root", type=str,
                                help="number or name of the feature/sample from which filtering should start",
                                default=None)
    parser_heatmap.add_argument("-f", "--feature", action="store_true",
                                help="use feature as a root")
    parser_heatmap.add_argument("-z", "--delete-zero", action="store_true",
                                help="delete zero columns")
    parser_heatmap.add_argument("-S", "--sort", action="store_true",
                                help="sort samples")
    parser_heatmap.add_argument("-ff", "--feature-from", type=int,
                                help="", default=0)
    parser_heatmap.add_argument("-ft", "--feature-to", type=int,
                                help="", default=None)
    parser_heatmap.add_argument("-sf", "--sample-from", type=int,
                                help="", default=0)
    parser_heatmap.add_argument("-st", "--sample-to", type=int,
                                help="", default=None)

    parser_network = subparsers.add_parser("network", parents=[parent_parser],
                                           help="Network chart (for help type 'run.py network --help')",
                                           description=description_network,
                                           formatter_class=argparse.RawDescriptionHelpFormatter)

    parser_network.add_argument("-p", '--positioning', type=str, help="Positioning algorithm s/m (default s)",
                                default="s")
    parser_network.add_argument("-r", "--root", type=str, default=0, help="Name or a number of sample/feature"
                                                                          "which will be used as a root of the "
                                                                          "network chart (default: 0)")

    parser_network.add_argument("-f", "--feature", action="store_true", help="if present, 'root' will be an id of "
                                                                             "a feature not a sample")
    parser_network.add_argument("-d", "--depth", type=int, help="Specify maximal depth of the generated"
                                                                " tree (default 2, dangerous parameter :) )", default=2)

    parser.add_argument("file", type=str, help="input file path")

    return parser


def main():
    options = {"reduction": generate_2d_point_chart,
               "bubble": generate_bubble_chart,
               "violin": generate_violin_chart,
               "network": generate_network_chart,
               "heatmap": generate_heatmap_chart}

    parser = get_initialized_parser()
    parser.parse_args()
    arguments = vars(parser.parse_args())

    data = dl.get_data_frame_from_file(parser.parse_args().file)
    if arguments["max_rows"] is not None and arguments["max_rows"] < len(data.index.values):
        data = data[:arguments["max_rows"]]

    options[arguments["which"]](data, **arguments)


if __name__ == "__main__":
    main()
