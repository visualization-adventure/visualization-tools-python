# visualization-tools-python

NOTE: Project uses Python3.5

1. Create virtualenv: `virtualenv -p python3.5 venv`
2. Launch virtualenv: `source venv/bin/activate`
3. Install requirements: `pip install -r requirements.txt`
4. Basic run: `python run.py --help`
    