import seaborn as sns
import matplotlib.pyplot as plt
import plotly.offline as py
import plotly.graph_objs as go


class DataVisualizer(object):

    def __init__(self, width=0, height=0):
        self.figure = plt.figure(1)
        self.ax = self.figure.add_subplot(111)
        self.width = width
        self.height = height
        self.rng = None
        self.names = None
        self.rotation = 0
        self.html = False

    def set_width_height(self, width, height):
        self.width = width
        self.height = height

    def set_x_ticks(self, rng, names, rotation):
        self.rng = rng
        self.names = names
        self.rotation = rotation

    def __reset(self):
        if self.width != 0 and self.height != 0 and not self.html:
            self.figure.set_size_inches(self.width, self.height)
        if self.rng is not None:
            plt.xticks(self.rng, self.names, rotation=self.rotation)

    def generate_2d_plot(self, df, labels, reg=True, as_html=True):
        temp_df = df.copy()
        temp_df["labels"] = labels

        if not self.html:
            plt.close(self.figure)

        if as_html:
            labels_set = set(labels)
            data = []
            for label in labels_set:
                partial_df = temp_df.loc[temp_df['labels'] == label]
                trace = go.Scatter(
                    x=partial_df["a"],
                    y=partial_df["b"],
                    mode='markers',
                    name=label
                )
                data.append(trace)

            self.figure = data
            self.html = True

        else:
            graph = sns.lmplot("a", "b", hue="labels", data=temp_df, fit_reg=reg, legend=False)
            self.figure = graph.fig
            self.ax = graph.axes
            self.html = False
            plt.grid(True)
            self.__reset()

    def generate_violin_plot(self, dt):
        if not self.html:
            plt.close(self.figure)
        self.html = False
        self.figure, self.ax = plt.subplots()
        self.__reset()
        plt.grid(True)
        sns.violinplot(x="column", y="row", data=dt, cut=0)

    def generate_heatmap_plot(self, dt):
        data = [go.Heatmap(x=dt.columns, y=dt.index, z=dt.values.tolist(), colorscale='Viridis')]

        self.figure = go.Figure(data=data)
        self.html = True

    def show_plot(self, blocking=True, html_name="output"):
        if self.html:
            py.plot(self.figure, filename=html_name + ".html")
        else:
            plt.figure(self.figure.number)
            plt.show(block=blocking)

    def save_plot(self, name, file_type="svg"):
        if self.html:
            py.plot(self.figure, filename=name + ".html", auto_open=False)
        else:
            self.figure.savefig(name + "." + file_type, format=file_type)

    def bubble(self, x, y, size, hover_text):
        tr = go.Scatter(
            x=x,
            y=y,
            mode='markers',
            name='Documents',
            text=hover_text,
            marker=dict(
                size=size,
                sizemode='area',
                sizeref=2. * max(size) / (40. ** 2),
                color=size,
                showscale=True
            )
        )
        data = [tr]
        layout = go.Layout(
            xaxis=dict(
                autorange=True,
                showgrid=False,
                zeroline=False,
                showline=False,
                autotick=True,
                ticks='',
                showticklabels=False
            ),
            yaxis=dict(
                autorange=True,
                showgrid=False,
                zeroline=False,
                showline=False,
                autotick=True,
                ticks='',
                showticklabels=False
            )
        )
        self.figure = go.Figure(data=data, layout=layout)
        self.html = True

    def generate_network_chart(self, graph, pos):

        edge_trace = go.Scatter(
            x=[],
            y=[],
            line=go.Line(width=0.5, color='#888'),
            hoverinfo='none',
            mode='lines')

        for edge in graph.edges():
            x0, y0 = pos[edge[0]]
            x1, y1 = pos[edge[1]]
            edge_trace['x'] += [x0, x1, None]
            edge_trace['y'] += [y0, y1, None]

        node_trace = go.Scatter(
            x=[],
            y=[],
            text=[],
            mode='markers',
            hoverinfo='text',
            marker=go.Marker(
                showscale=True,
                colorscale='YIGnBu',
                reversescale=True,
                color=[],
                size=10,
                colorbar=dict(
                    thickness=15,
                    title='Node Connections',
                    xanchor='left',
                    titleside='right'
                ),
                line=dict(width=2)))

        for node in graph.nodes():
            x, y = pos[node]
            node_trace['x'].append(x)
            node_trace['y'].append(y)
            info = node + "<br><br>Neighbors:<br>" + "<br>".join(graph.neighbors(node))
            node_trace['text'].append(info)
            if graph.nodes[node]["type"] == "sample":
                node_trace["marker"]["color"].append("rgb(178, 12, 0)")
            else:
                node_trace["marker"]["color"].append("rgb(0, 178, 77)")

        fig = go.Figure(data=go.Data([edge_trace, node_trace]),
                        layout=go.Layout(
                            title='<br>Network graph made with Python',
                            titlefont=dict(size=16),
                            showlegend=False,
                            hovermode='closest',
                            margin=dict(b=20, l=5, r=5, t=40),
                            annotations=[dict(
                                showarrow=False,
                                xref="paper", yref="paper",
                                x=0.005, y=-0.002)],
                            xaxis=go.XAxis(showgrid=False, zeroline=False, showticklabels=False),
                            yaxis=go.YAxis(showgrid=False, zeroline=False, showticklabels=False)))
        self.figure = fig
        self.html = True
