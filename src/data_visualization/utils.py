import itertools
from string import ascii_lowercase
import src.data_visualization.data_visualizer as dv
import src.data_preparation.data_preprocessor as dp


def iter_all_strings():
    size = 1
    while True:
        for s in itertools.product(ascii_lowercase, repeat=size):
            yield "".join(s)
        size += 1


def generate_series_of_violin_plots(data, name_prefix="", size_x=40, size_y=10):
    columns_per_image = int(size_x / 0.2)
    list_of_rows = list(data.index)
    visualiser = dv.DataVisualizer()
    visualiser.set_width_height(size_x, size_y)
    visualiser.set_x_ticks(range(0, len(data.columns)), data.columns, rotation=90)
    letter_gen = iter_all_strings()

    while len(list_of_rows) != 0:
        name = next(letter_gen)
        row_names = []
        start = 0
        stop = columns_per_image
        for i in range(0, int(size_y / 3)):
            if len(list_of_rows) != 0:
                row_names.append(list_of_rows.pop())

        while start < len(data.columns):
            part_of_data = dp.get_row_column_id_df_for_violin_plot(data, row_names, start, stop)
            visualiser.generate_violin_plot(part_of_data)
            visualiser.save_plot(name_prefix + name + str(start) + "-" + str(stop), "png")
            start += columns_per_image
            stop += columns_per_image
            if stop > len(data.columns):
                stop = len(data.columns)


def get_params_for_bubble_chart(dt):
    size = []
    x = dt.columns
    y = []
    hover_text = []
    i = 0
    for name, values in dt.iteritems():
        partial_results = {}
        all_values = 1.0
        for n, v in dt[name].iteritems():
            document_general = n.split('_')[0]
            partial_results[document_general] = partial_results.get(document_general, 0.0) + v
            all_values = all_values + v
        y.append(i % 2500)
        i = i + 100
        size.append(all_values)
        feature_text = ''
        for d in partial_results:
            feature_text = feature_text + '{}: {}%<br>'.format(d, (partial_results[d]/all_values)*100)
        hover_text.append(('Feature: {feature} ({all})<br>' + '{v}').format(feature=name,
                                                                            all=all_values, v=feature_text))
    return x, y, size, hover_text
