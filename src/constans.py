description_reduction = '''
    
    Available reduction algorithms: 
    +-------------+---------------------------------------------+
    | Option name |               Algorithm name                |
    +-------------+---------------------------------------------+
    | pca         | Principal component analysis (default)      |
    | fa          | Factor Analysis                             |
    | nmf         | Non-Negative Matrix Factorization           |
    | spca        | Sparse Principal component analysis         |
    | tsvd        | Truncated Singular Value Decomposition      |
    | kpca        | Kernel Principal component analysis         |
    | ipca        | Incremental Principal component analysis    |
    | fica        | Fast Independent Component Analysis         |
    | tsne        | t-distributed Stochastic Neighbor Embedding |
    | mds         | Multidimensional Scaling                    |
    | isomap      | Isomap Embedding                            |
    | se          | Spectral Embedding                          |
    | lle         | Locally Linear Embedding                    |
    +-------------+---------------------------------------------+
    
    Available clustering methods:
    +-------------+-------------------------------------------------------------+
    | Option name |                         Method name                         |
    +-------------+-------------------------------------------------------------+
    | km          | K-Means Clustering                                          |
    | mbkm        | Mini Batch K-Means                                          |
    | ap          | Affinity Propagation                                        |
    | ms          | Mean Shift                                                  |
    | sc          | Spectral Clustering                                         |
    | ac          | Agglomerative Clustering                                    |
    | dbscan      | Density-Based Spatial Clustering of Applications with Noise |
    | gm          | Gaussian Mixture                                            |
    | birch       | Birch                                                       |
    +-------------+-------------------------------------------------------------+
    
    
    Examples:
    
    python run.py reduction -r pca input.json
    python run.py reduction -r mds -s -c km input.json'''

description_network = '''

    +-------------+------------------------------------------------+
    | Option name |                  Method name                   |
    +-------------+------------------------------------------------+
    | s (default) | Fruchterman-Reingold force-directed algorithm. |
    | k           | Kamada-Kawai path-length cost-function         |
    +-------------+------------------------------------------------+

    Examples:
    
    python run.py network -r 1 -s -p m input.json
    python run.py network -r base:AF -f input.json'''
