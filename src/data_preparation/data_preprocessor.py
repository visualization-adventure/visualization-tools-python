from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from sklearn.decomposition import NMF
from sklearn.decomposition import SparsePCA
from sklearn.decomposition import TruncatedSVD
from sklearn.decomposition import KernelPCA
from sklearn.decomposition import IncrementalPCA
from sklearn.decomposition import FastICA
from sklearn.decomposition import FactorAnalysis
from sklearn.manifold import TSNE
from sklearn.manifold import MDS
from sklearn.manifold import Isomap
from sklearn.manifold import SpectralEmbedding as SE
from sklearn.manifold import LocallyLinearEmbedding as LLE

from sklearn.cluster import KMeans, \
    MiniBatchKMeans, \
    AffinityPropagation, \
    MeanShift, \
    SpectralClustering, \
    AgglomerativeClustering, \
    DBSCAN, \
    Birch

from sklearn.mixture import GaussianMixture

import pandas as pd
import re
import inspect
import networkx as nx
import numpy as np

reduction_algorithms = {"pca": PCA,
                        "fa": FactorAnalysis,
                        "nmf": NMF,
                        "spca": SparsePCA,
                        "tsvd": TruncatedSVD,
                        "kpca": KernelPCA,
                        "ipca": IncrementalPCA,
                        "fica": FastICA,
                        "tsne": TSNE,
                        "mds": MDS,
                        "isomap": Isomap,
                        "se": SE,
                        "lle": LLE}

clustering_algorithms = {"km": KMeans,
                         "mbkm": MiniBatchKMeans,
                         "ap": AffinityPropagation, ##no n_clusters
                         "ms": MeanShift, #no n_clusters
                         "sc": SpectralClustering,
                         "ac": AgglomerativeClustering,
                         "dbscan": DBSCAN, #no_n_cliusters
                         "gm": GaussianMixture,
                         "birch": Birch}


def is_int(s):
    try:
        int(s)
        return True
    except ValueError:
        return False


def get_labels(df, **kwargs):
    data = df.copy()
    kwargs["n_components"] = kwargs["n_clusters"]
    args = filter_dict(kwargs, clustering_algorithms[kwargs["clustering_method"]])
    model = clustering_algorithms[kwargs["clustering_method"]](**args).fit(data)
    return model.labels_ if kwargs["clustering_method"] != "gm" else model.predict(data)


def get_reduced_data(df, **kwargs):
    standardise = False if kwargs["reduction_method"] == "nmf" else True
    x = StandardScaler().fit_transform(df) if standardise else df[:]
    args = filter_dict(kwargs, reduction_algorithms[kwargs["reduction_method"]])
    result = reduction_algorithms[kwargs["reduction_method"]](**args).fit_transform(x)
    result = pd.DataFrame(data=result, columns=['a', 'b'])

    pattern = "_(\d+).*"
    list_of_index = [re.sub(pattern, "", x) for x in list(df.index.values)]
    labels = list_of_index

    return result, labels


def remove_rows_without_given_property(df, col):
    col = int(col) if is_int(col) else df.columns.get_loc(col)

    for row_name, row in df.iterrows():
        if df.loc[row_name][col] == 0:
            df = df.drop(row_name)

    return df


def remove_cols_without_given_property(df, row):
    if is_int(row):
        row = df.index.tolist()[int(row)]

    for col in df:
        if df.loc[row, col] == 0:
            df.drop(col, axis=1, inplace=True)

    return df


def remove_zero_columns(df):
    return df.loc[:, (df != 0).any(axis=0)]


def get_row_column_id_df_for_violin_plot(df, rows=None, from_column=0, to_column=None):
    result = pd.DataFrame()
    names = ["row", "column"]

    if to_column is None or to_column > len(df.columns):
        to_column = len(df.columns)

    if from_column > to_column:
        return None

    if rows is None or len(rows) == 0:
        rows = list(df.index)

    if is_int(rows[0]):
        rows = [df.index[int(x)] for x in rows]

    for row in rows:
        for column_num in range(from_column, to_column):
            for i in range(0, int(df.loc[row][column_num])):
                tup = (row, column_num)
                temp = pd.DataFrame([tup], columns=names)
                result = result.append(temp)

    return result


def filter_dict(dict_to_filter, thing_with_kwargs):
    sig = inspect.signature(thing_with_kwargs)
    function_arguments = [param.name for param in sig.parameters.values()]
    filtered_dict = {key: dict_to_filter[key] for key in dict_to_filter if key in function_arguments}
    return filtered_dict


def get_edges_from_row(df, row):
    graph = nx.Graph()
    sub_roots = []

    if isinstance(row, int):
        row = df.index[row]
    selected_row = df.loc[row]

    graph.add_node(row, type="sample")

    for column in df.columns:
        if selected_row[column] > 0:
            graph.add_node(column, type="feature")
            graph.add_edge(row, column, weight=selected_row[column])
            sub_roots.append(column)

    return graph, sub_roots


def get_edges_from_column(df, column):
    graph = nx.Graph()
    sub_roots = []

    if isinstance(column, int):
        column = df.columns[column]
    selected_column = df[column]

    graph.add_node(column, type="feature")

    for row in df.index:
        if selected_column[row] > 0:
            graph.add_edge(row, column, weight=selected_column[row])
            graph.add_node(row, type="sample")
            sub_roots.append(row)

    return graph, sub_roots


def get_edges(dt, root, depth=3, method="k", feature=False):
    if is_int(root):
        root = int(root)
        if feature:
            if len(dt.columns) <= root:
                raise IndexError
            root = dt.columns[root]
        else:
            if len(dt.index) <= root:
                raise IndexError
            root = dt.index[root]
    else:
        if feature:
            if root not in dt.columns:
                raise IndexError
        else:
            if root not in dt.index:
                raise IndexError

    data = dt.copy()
    sub_roots = {root}
    new_roots = set()
    graph = nx.Graph()
    offset = 1 if feature else 0

    for i in range(offset, depth + offset):

        if not sub_roots:
            break
        for sub_root in sub_roots:
            if i % 2 == 0:
                partial_graph, new_roots_part = get_edges_from_row(data, sub_root)
                data = data.drop(sub_root)
            else:
                partial_graph, new_roots_part = get_edges_from_column(data, sub_root)
                data.pop(sub_root)

            new_roots = new_roots.union(set(new_roots_part))
            graph = nx.compose(graph, partial_graph)

        sub_roots = new_roots
        new_roots = set()

    if method == "s":
        pos = nx.spring_layout(graph)
    else:
        pos = nx.kamada_kawai_layout(graph)

    return graph, pos


def cut_matrix(data, x_from=0, x_to=None, y_from=0, y_to=None):
    if not y_to or y_to >= len(data.index):
        y_to = len(data.index) - 1

    if not x_to or x_to >= len(data.columns):
        x_to = len(data.columns) - 1

    if x_from < 0:
        x_from = 0

    if y_from < 0:
        y_from = 0

    if x_from < x_to:
        columns_to_drop = data.columns[:x_from].tolist() + data.columns[x_to:].tolist()
        data.drop(columns_to_drop, axis=1, inplace=True)
    else:
        print("Invalid x_from or x_to parameters - omitting")

    if y_from < y_to:
        data = data[y_from:y_to]
    else:
        print("Invalid y_from or y_to parameters - omitting")

    return data
