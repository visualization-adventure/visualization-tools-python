import json
import pandas as pd


def read_json_file(file_name):
    with open(file_name, encoding="utf-8") as data_file:
        data = json.loads(data_file.read())

    return data


def get_data_frame_from_json(json_str):

    data_key = next(x for x in json_str.keys() if isinstance(json_str[x][0], list))
    keys = {data_key}
    rows_key = next(x for x in json_str.keys() if len(json_str[data_key]) == len(json_str[x]) and x not in keys)
    keys.add(rows_key)
    columns_key = next(x for x in json_str.keys() if len(json_str[data_key][0]) == len(json_str[x]) and x not in keys)

    return pd.DataFrame(json_str[data_key], columns=json_str[columns_key], index=json_str[rows_key])


def get_data_frame_from_file(file):
    json_str = read_json_file(file)
    return get_data_frame_from_json(json_str)


if __name__ == "__main__":
    print(get_data_frame_from_file("../../data/input.json"))
